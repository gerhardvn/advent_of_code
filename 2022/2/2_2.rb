require 'pry'

def score_round(them, me)
  round_score = 1
  sum = them - me
  case (sum)
    when 0     #draw
      round_score += 3 + me
    when -1, 2 #win
      round_score += 6 + me
    when 1, -2 #lose
      round_score += me
  end
  # binding.pry
  # puts round_score
  round_score
end

def my_action(them, me)
  new_me = them #draw
  case (me)
    when 2 #win
      new_me += 1
    when 0 #lose
      new_me -= 1
  end
  if (new_me < 0)
    new_me = 2
  elsif (new_me > 2)
      new_me = 0
  end 
  new_me  
end

score = 0

File.readlines('input.txt', chomp: true).each do |line|
  them = line[0].ord - 'A'.ord
  me = line[2].ord - 'X'.ord
  print "#{them}-#{me}"
  me = my_action(them, me)
  print ": #{them}-#{me}"
  score += score_round(them, me)
  puts ": #{score}"
end

puts score