require 'pry'

def score_round(them, me)
  round_score = 1
  sum = them - me
  case (sum)
    when 0     #draw
      round_score += 3 + me
    when -1, 2 #win
      round_score += 6 + me
    when 1, -2 #lose
      round_score += me
  end
  # binding.pry
  # puts round_score
  round_score
end

score = 0

File.readlines('input.txt', chomp: true).each do |line|
  them = line[0].ord - 'A'.ord
  me = line[2].ord - 'X'.ord
  score += score_round(them, me)
end

puts score