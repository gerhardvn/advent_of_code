def score(n)
    priority = n.ord - 'A'.ord
    if n.between?('A', 'Z') 
      priority += 27
    else
      priority -= 31
    end 
    priority
end

score = 0

File.readlines('input.txt', chomp: true).each do |line|
  first, second = line.chars.each_slice(line.length/2).to_a()
  common = first & second
  common.each do |n|

    score += score(n)
  end
end

puts score