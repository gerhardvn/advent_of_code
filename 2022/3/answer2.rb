def score(n)
    priority = n.ord - 'A'.ord
    if n.between?('A', 'Z') 
      priority += 27
    else
      priority -= 31
    end 
    priority
end

score = 0

File.readlines('input.txt', chomp: true).each_slice(3) do |lines|
  first, second, third = lines
  common = first.chars & second.chars & third.chars
  common.each do |n|
    score += score(n)
  end
end

puts score