count = 0
max_three = [0, 0, 0]

File.readlines('input.txt', chomp: true).each do |line|
    if line.empty?
      max_three << count
      puts max_three.inspect
      max_three.sort!.reverse!.pop
      count = 0      
    else
      count += line.to_i()
    end
end
puts max_three.sum