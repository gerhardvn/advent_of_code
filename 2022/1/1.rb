count = 0
max = 0
File.readlines('input.txt', chomp: true).each do |line|
    if line.empty?
      max = [max, count].max
      count = 0      
    else
      count += line.to_i()
    end
end
puts max