require 'set'

# First, let's make some nodes
def find_group_size(graph, start_node)
  visited = Set.new

  # recursion with same scope!
  search = lambda do |node|
    unless visited.include?(node)
      visited.add(node)
      graph[node].each { |neighbor| search.call(neighbor) }
    end
  end

  search.call(start_node)
  visited.size
end

input = []
File.readlines('input1.txt', chomp: true).each do |line|
  numbers = line.scan(/\d+/)
  input.push(numbers[1 .. -1].map(&:to_i))
end

group_size = find_group_size(input, 0)
puts "#{group_size}"