require 'set'

def find_group_size(graph, start_node, visited)
  return if visited.include?(start_node)

  visited.add(start_node)
  graph[start_node].each { |neighbor| find_group_size(graph, neighbor, visited) }
end

def count_groups(graph)
  visited = Set.new
  groups = 0

  graph.keys.each do |start_node|
    unless visited.include?(start_node)
      find_group_size(graph, start_node, visited)
      groups += 1
    end
  end

  groups
end


input = {}
File.readlines('input1.txt', chomp: true).each do |line|
  numbers = line.scan(/\d+/)
  input[numbers[0].to_i] = numbers[1 .. -1].map(&:to_i)
end

groups = count_groups(input)
puts "#{groups}"
