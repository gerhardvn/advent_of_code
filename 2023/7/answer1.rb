def get_hand_rank(hand)
  hand_rankings = {
    'High Card' => 0,
    'One Pair' => 1,
    'Two Pair' => 2,
    'Three of a Kind' => 3,
    'Full House' => 4,
    'Four of a Kind' => 5,
    'Five of a Kind' => 6
  }  

  tidy = hand.chars.sort().join()
  # aliasing so keep order
  case tidy
  when /(\w)\1{4}/
    score = 'Five of a Kind'
  when /(\w)\1{3}/
    score = 'Four of a Kind'
  when /^(\w)\1{1,2}(\w)\2{1,2}$/
    score = 'Full House'
  when /(\w)\1{2}/
    score = 'Three of a Kind'
  when /(\w)\1{1}\w?(\w)\2{1}/
    score = 'Two Pair'
  when /(\w)\1{1}/
    score = 'One Pair'
  else
    score = 'High Card'
  end
  return hand_rankings[score]
end
  
def custom_string_compare(cards1, cards2)
  5.times do |index|
    score1 = '23456789TJQKA'.index(cards1.chars[index])
    score2 = '23456789TJQKA'.index(cards2.chars[index])
    if score1 < score2
      return -1
    elsif score1 > score2
      return 1
    end
  end
  return 0
end


def compare_hands(cards1,cards2)
  score1 = get_hand_rank(cards1)
  score2 = get_hand_rank(cards2)

  if score1 < score2
    -1
  elsif score1 > score2
    1
  else
    custom_string_compare(cards1, cards2)
  end  
end


hands = Array.new
File.open('input.txt', chomp: true).each do |line|
  cards, bid = line.scan(/(\S{5})\s(\d{1,4})/)[0]
  hands << [cards, bid]
end

hands.sort! do |hand1, hand2|
  cards1, bid1 = hand1
  cards2, bid2 = hand2
  score = compare_hands(cards1,cards2)
  score
end

puts hands.inspect

scores = 0
hands.each_with_index do |hand, index|
  cards, bid = hand
  scores += bid.to_i * (index + 1)
end

puts scores