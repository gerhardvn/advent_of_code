def get_hand_rank(hand)
  #puts hand.inspect
  num_unique_cards = hand.chars.uniq.length
  num_jokers = hand.count('J')
  if num_jokers == nil
    num_jokers = 0
  end
  unique_map = hand.chars.group_by(&:itself).map { |_char, instances| instances.length }
  score = 0
  if num_unique_cards == 1 ||
    ((num_jokers > 0) &&  (num_unique_cards == 2))
    score = 1
  elsif (num_unique_cards == 2) || (num_unique_cards == 3 && num_jokers == 1)
    score = 2
  elsif num_unique_cards == 2
    score = 3
  elsif (num_unique_cards == 3) && unique_map.include?(3 - num_jokers)
    score = 4
  elsif num_unique_cards == 3 || (num_unique_cards == 4 && num_jokers == 1)
    score = 5
  elsif num_unique_cards == 4 || num_jokers == 1
    score = 6
  else
    score = 7
  end
  return score
end

def custom_string_compare(cards1, cards2)
  5.times do |index|
    score1 = 'J23456789TQKA'.index(cards1.chars[index])
    score2 = 'J23456789TQKA'.index(cards2.chars[index])
    if score1 < score2
      return -1
    elsif score1 > score2
      return 1
    end
  end
  return 0
end

def compare_hands(cards1,cards2)
  score1 = get_hand_rank(cards1)
  score2 = get_hand_rank(cards2)
  
  #puts "#{cards1} #{score1} #{cards2} #{score2}"
  
  if score1 > score2
    score = -1
  elsif score1 < score2
    score = 1
  else
    score = custom_string_compare(cards1,
                                  cards2)
  end  
  #puts "#{cards1},#{cards2}:#{score}"
  score
end


hands = Array.new
File.open('input.txt', chomp: true).each do |line|
  cards, bid = line.scan(/(\S{5})\s(\d{1,4})/)[0]
  hands << [cards, bid]
end

#puts hands.inspect

hands.sort! do |hand1, hand2|
  cards1, bid1 = hand1
  cards2, bid2 = hand2
  score = compare_hands(cards1,cards2)
  score
end

puts "-------"
hands.each {|hand| puts hand[0]}

scores = 0
hands.each_with_index do |hand, index|
  cards, bid = hand
  scores += bid.to_i * (index + 1)
end

puts scores