score = 0

lines = []
File.readlines('input.txt', chomp: true).each do |line|
  lines << line
end

steps = lines[0]

input_regex = /^(\w{3})\W{4}(\w{3})\W{2}(\w{3})\W$/
start_node = "AAA"
end_node = "ZZZ"

#puts "nav: #{start_node} -> #{end_node}"

map = {}
(2 .. (lines.length - 1)).each do |line|
  x = lines[line].scan(/^(\w{3})\W{4}(\w{3})\W{2}(\w{3})\W$/)[0]
  map[x[0]] = x[1..2]
end

puts map.inspect
current_node = start_node
puts current_node.inspect
while 1
  steps.chars.each do |step|
    if step == "L"
      #puts "#{current_node} => #{map[current_node][0]}"
      current_node = map[current_node][0]
      
    else
      #puts "#{current_node} => #{map[current_node][0]}"
      current_node = map[current_node][1]
    end
    score += 1
    #puts "#{current_node} == #{end_node} = #{current_node == end_node}"
    if current_node == end_node
      break
    end
  end
  if current_node == end_node
    break
  end
end

puts score
