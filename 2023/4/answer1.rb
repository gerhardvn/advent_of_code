def scoring(wins)
  if wins > 2
      wins = (2 ** wins) /2
  end
  wins       
end  

score = 0

File.readlines('input.txt', chomp: true).each do |line|
  x = line.scan(/Card\s+\d{1,3}:\s{1,2}((\d{1,3}\s*){1,15})\|\s{1,2}((\d{1,2}\s*){1,32})/)[0]
  puts line
  winning = x[0].split(' ').sort_by(&:to_i)
  mine = x[2].split(' ').sort_by(&:to_i)
  my_winning = winning & mine
  puts winning.inspect, mine.inspect
  puts my_winning.inspect, my_winning.length
  s = scoring(my_winning.length)
  puts s
  score += s
  puts "------"
end

puts score