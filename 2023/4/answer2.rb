score = 0
next_takes = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

File.readlines('input.txt', chomp: true).each do |line|
  takes = next_takes.shift
  next_takes << 0
  score += 1 + takes
  
  x = line.scan(/Card\s+\d{1,3}:\s{1,2}((\d{1,3}\s*){1,15})\|\s{1,2}((\d{1,2}\s*){1,32})/)[0]
  winning = x[0].split(' ').sort_by(&:to_i)
  mine = x[2].split(' ').sort_by(&:to_i)
  my_winning = winning & mine
  my_winning.length().times {|x| next_takes[x] += 1 + takes}
end

puts score