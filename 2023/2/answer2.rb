
score = 0

File.readlines('input.txt', chomp: true).each do |line|
  game = line.scan(/Game\s(\d+):/).join().to_i
  hands = line.scan(/\d{1,3}\s\w{3,5}/)
  hand_detail = { "blue" => 0, "red" => 0, "green" => 0 }
  hands.each do |hand|
    number, color = hand.scan(/(\d{1,3})\s(\w{3,5})/)[0]
    hand_detail[color] = [number.to_i, hand_detail[color]].max()
  end
  n = 1
  hand_detail.each_value do |x|
    n *= x
  end
  score += n
  
end

puts score