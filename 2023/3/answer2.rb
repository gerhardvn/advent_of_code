def find_gears(grid)
  gear_regex = /\*{1}/
  last = grid.length() -1
  length = grid[0].length() -1
  gear = []
  
  for a in 0..last do 
    puts "------------------------------------------"
    top = ""
    bottom = ""

    grid[a].scan(/\*{1}/) do |match|
      # Find gear
      start_char, end_char = Regexp.last_match.offset(0)
      if (start_char < 3)
        start, stop = [0, start_char + 3]
      elsif ((start_char + 3) > length)        
        start, stop = [(start_char - 3), length]
      else
        start, stop = [(start_char - 3),(start_char + 3)]
      end
      
      # gear picture
      line = grid[a][start..stop]
      if (a > 0)
        top = grid[a-1][start..stop]
      end
      if (a < last)
        bottom = grid[a+1][start..stop]
      end
      
      # parse
      # inline
      m = line.match(/(\d{1,3})\*(\d{1,3})/)
      if m
        gear << m[1].to_i * m[2].to_i
        next
      end
      
      #next
      
   end  
  end
  gear
end

# ----------------------------------------------------------------------------
score = 0

lines = Array.new
File.open('input1.txt', chomp: true).each { |line| lines << line.chomp }
part_numbers = find_gears(lines)
part_numbers.each {|x| score += x.to_i}
puts score