def find_adjacent(grid)

  adjacent_elements = []
  chars_regex = /[~`!@#$%^&*()_+={}\[\]:;'"\/\\?><,-]/
  last = grid.length() -1
  
  for a in 0..last do
    
    #puts grid[a]
    grid[a].scan(/\d{1,4}/) do |match|
      start_char, end_char = Regexp.last_match.offset(0)
      top = ""
      bottom = ""
      
      if (a > 0)
        if (start_char > 0) then
          top = grid[a - 1].chars[(start_char -1)..end_char].join
        else
          top = grid[a - 1].chars[start_char..end_char].join
        end
      end
      
      if (a != last)
        if (start_char > 0) then
          bottom = grid[a + 1].chars[(start_char -1)..end_char].join
        else
          bottom = grid[a + 1].chars[start_char..end_char].join
        end
      end
      
      if (start_char > 0) and (grid[a].chars[start_char -1] =~ chars_regex)
        puts "#{match} left"
        adjacent_elements << match
      elsif (grid[a].chars[end_char] =~ chars_regex)
        puts "#{match} rigth"
        adjacent_elements << match
      elsif top =~ chars_regex
        puts "#{match} top"
        adjacent_elements << match
      elsif bottom =~ chars_regex
        puts "#{match} bottom"
        adjacent_elements << match
      else
        puts "#{match} nothing"
      end
      
    end
  end
  
  adjacent_elements
end

# ----------------------------------------------------------------------------
score = 0

lines = Array.new
File.open('input.txt', chomp: true).each { |line| lines << line.chomp }
part_numbers = find_adjacent(lines)
part_numbers.each {|x| score += x.to_i}
puts score