def find_gear_ratio(input)
  input.each_with_index.sum do |line, index|
    line_sum = 0
    gears = regex_with_index(line, /(\*)/)
    if (gears.length == 0)
      next 0 
    end
    
    if not (index == 0)
      above = regex_with_index(input[index - 1], /(\d+)/)
    end
    if not (index >= input.length - 1)
      below = regex_with_index(input[index + 1], /(\d+)/)
    end  
    same_line = regex_with_index(line, /(\d+)/)

    gear_context = same_line
    gear_context += below if below 
    gear_context += above if above

    gears.each do |gear|
      ratios = []

      gear_context.each do |numbers|
        if (numbers[:index] - 1..numbers[:index] + numbers[:num].length).include?(gear[:index])
          ratios << numbers[:num] 
        end
      end

      if (ratios.length == 2)
        line_sum += ratios.map(&:to_i).inject(:*) 
      end
    end
    line_sum
  end
end

def regex_with_index(line, pattern)
  result = []

  line.scan(pattern) do |num_match|
    num = num_match.first
    left = Regexp.last_match.begin(0)
    result << { num: num, index: left } #multiple matches
  end
  result
end

# ----------------------------------------------------------------------------
lines = Array.new
File.open('input.txt', chomp: true).each { |line| lines << line.chomp }
puts find_gear_ratio(lines)
