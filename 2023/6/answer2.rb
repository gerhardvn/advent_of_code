input = [[53897698,313109012141201]]

def time_distance(time, total_time)
  distance = time * (total_time - time)
end

score = 1
input.each do |td|
  round_score = 0
  time, distance = td
  (1 .. time).each do |i|
    my_distance = time_distance(i, time)
    if my_distance > distance
      #puts "#{time} #{i} #{my_distance}"
      round_score += 1
    end
  end
  #puts round_score
  score *= round_score
  #puts "------------------------"
end

puts score
