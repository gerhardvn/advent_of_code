input = [[53,313], [89, 1090], [76,1214], [98, 1201]]
#input = [[7,9],[15,40],[30,200]]

def time_distance(time, total_time)
  distance = time * (total_time - time)
end

score = 1
input.each do |td|
  round_score = 0
  time, distance = td
  time.times do |i|
    my_distance = time_distance(i, time)
    if my_distance > distance
      puts "#{time} #{i} #{my_distance}"
      round_score += 1
    end
  end
  puts round_score
  puts "------------------------"
  score *= round_score
end

puts score