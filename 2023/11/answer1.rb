input = File.read("input.txt")
grid = input.each_line.map { |n| n.strip.chars }

width = grid.first.count
height = grid.count

y_expand = height.times.select { |n| grid[n].join.match?(/\A\.*\z/)}
transposed_grid = grid.transpose()
x_expand = width.times.select { |n| transposed_grid[n].join.match?(/\A\.*\z/)}
y_expand.each_with_index { |row, idx| grid.insert(row + idx, Array.new(width, "."))}

height = grid.count
grid = grid.transpose
x_expand.each_with_index { |row, idx| grid.insert(row + idx, Array.new(height, "."))}

grid = grid.transpose
width = grid.first.count

x = height.times.to_a
    .product(width.times.to_a)
    .select { |x ,y| grid[x][y] == ?# }
    .map { |x ,y| [y, x] }
    .combination(2)
    .sum { |(x1, y1), (x2, y2)| (x1 - x2).abs + (y1 - y2).abs }

puts x