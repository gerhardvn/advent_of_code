$y_direction = 0
$x_direction = 0

def up
   $y_direction == -1
end

def move_up
  puts "Up"
  $y_direction = -1
  $x_direction = 0
  -1
end

def down
   $y_direction == 1
end

def move_down
   puts "down"
   $y_direction = 1
   $x_direction = 0
   1
end

def left
  $x_direction == -1
end

def move_left
  puts "Left"
  $y_direction = 0
  $x_direction = -1
  -1
end

def right
  $x_direction == 1
end

def move_right
  puts "Right"
  $y_direction = 0
  $x_direction = 1
  1
end


def next_position(x,y, lines) 
  if lines[y][x] == "S"
    if lines[y + 1][x] != "."
      y += move_down
    elsif lines[y -1][x] != "."
      y += move_up
    elsif lines[y][x + 1] != "."
      x += move_rigth
    else
      x += move_left   
    end
  else
    case lines[y][x]
    when ("|") 
      if up
        y += move_up
      elsif down
        y += move_down
      else
        puts "Fubar #{x}, #{y} = #{lines[y][x]}"
        exit
      end
    when ("-") 
      if left
        x += move_left
      elsif right
        x+= move_right
      else
        puts "Fubar #{x}, #{y} = #{lines[y][x]}"
        exit
      end
    when "J"
      if right
        y += move_up
      elsif down
        x += move_left
      else
        puts "Fubar #{x}, #{y} = #{lines[y][x]}"
        exit
      end
    when "F"
      if left
        y += move_down
      elsif up
        x += move_right
      else
        puts "Fubar #{x}, #{y} = #{lines[y][x]}"
        exit
      end
    when "L"
      if left
        y += move_up
      elsif down
        x += move_right
      else
        puts "Fubar #{x}, #{y} = #{lines[y][x]}"
        exit
      end
    when "7"
      if right
        y += move_down 
      elsif up
        x += move_left
      else
        puts "Fubar #{x}, #{y} = #{lines[y][x]}"
        exit
      end
    else
      puts "Fubar #{x}, #{y} = #{lines[y][x]}"
      exit
    end
  end
  return [x, y]
end

# ----------------------------------------------------------------------
lines = Array.new
File.open('input.txt', chomp: true).each { |line| lines << line.chomp }

puts lines
x = 0
y = 0

(lines.length() -1).times do |i|
  if lines[i].include? "S"
    x = lines[i].index('S')
    y = i    
    break
  end
end

puts x.inspect, y.inspect
score = 0

begin 
 x,y = next_position(x,y,lines) 
 puts x.inspect, y.inspect
 score += 1
end while (lines[y][x] != "S")

puts score/2
