@file_location = 3
  
def get_section_from_file(lines)
  temp = Array.new
  until (lines[@file_location] == nil) or (lines[@file_location].length < 2)
    temp << lines[@file_location].scan(/(\d{1,10})/).flatten().map(&:to_i)
    @file_location += 1
  end
  @file_location += 2  
  temp
end

def inrange(source,range,seed)
  if (source > seed) or (seed > (source + range))
    return false
  end
  return true
end

def mapper(seed, map)
  puts seed
  new_seed = -1
  map.each do |m|
    puts m.inspect
    new_seed = mini_mapper(m[0],m[1],m[2], seed)
    if new_seed != -1
      break
    end      
  end
  if new_seed == -1
    puts "-> #{seed}"
    new_seed = seed
  end 
  new_seed
end

def mini_mapper(source,target,range,seed)
  x = 0
  ans = -1
  if inrange(source,range,seed)
    if (source >= target)
      ans = source - target + seed
    else
      ans = target - source + seed
    end
    puts " -> #{ans}"
  end
  ans
end

# ----------------------------------------------------------------------
lines = Array.new
File.open('input1.txt', chomp: true).each { |line| lines << line.chomp }

seeds = lines[0].scan(/(\d{1,10})/).flatten().map(&:to_i)
seed_to_soil = get_section_from_file(lines)
soil_to_fertilizer = get_section_from_file(lines)
fertilizer_to_water = get_section_from_file(lines)
water_to_light = get_section_from_file(lines)
light_to_temperature = get_section_from_file(lines)
temperature_to_humidity = get_section_from_file(lines)
humidity_to_location = get_section_from_file(lines)

answers = []

seeds.each do |seed|
  seed_to_soil_anwser = mapper(seed, seed_to_soil)
  puts seed_to_soil_anwser
  soil_to_fertilizer_answer =  mapper(seed_to_soil_anwser, soil_to_fertilizer)
  puts soil_to_fertilizer_answer
  fertilizer_to_water_answer = mapper(soil_to_fertilizer_answer, fertilizer_to_water)
  puts fertilizer_to_water_answer
  water_to_light_answer = mapper(fertilizer_to_water_answer, water_to_light)
  puts water_to_light_answer
  light_to_temperature_answer = mapper(water_to_light_answer, light_to_temperature)
  puts light_to_temperature_answer
  temperature_to_humidity_answer = mapper(light_to_temperature_answer, temperature_to_humidity)
  puts temperature_to_humidity_answer
  humidity_to_location_answer = mapper(temperature_to_humidity_answer, humidity_to_location)
  puts humidity_to_location_answer
  puts "#{seed}->#{humidity_to_location_answer}", "----------------------------------------"
  answers << humidity_to_location_answer
end

puts seeds.inspect
puts answers.inspect