def text_to_digit(text)
  text_mapping = {
    'zero' => '0',
    'one' => '1',
    'two' => '2',
    'three' => '3',
    'four' => '4',
    'five' => '5',
    'six' => '6',
    'seven' => '7',
    'eight' => '8',
    'nine' => '9'
  }

  if text_mapping.key?(text)
    return text_mapping[text]
  else
    return text
  end
end

score = 0

File.readlines('input.txt', chomp: true).each do |line|
  numbers = line.scan(/(?=(\d|zero|one|two|three|four|five|six|seven|eight|nine))/).values_at(0,-1)
  first = text_to_digit(numbers[0][0])
  last = text_to_digit(numbers[1][0])
  score += (first + last).to_i()
  puts line, first, last
end

puts score