score = 0

File.readlines('input.txt', chomp: true).each do |line|
  first = line[/\d/]
  last = line[/\d(?=\D*$)/]
  number_string = first + last
  score += number_string.to_i()
end

puts score